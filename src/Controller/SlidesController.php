<?php

namespace Drupal\slides\Controller;

use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class SlidesController {

    public function index($image_style) {

        $style = ImageStyle::load($image_style.'_slide');

        if (!$style) return new NotFoundHttpException();

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'slides', 'field_slide_enable' => true]);

        usort($terms, function ($a, $b) {
            if ($a->getWeight() == $b->getWeight()) {
                return 0;
            }

            return ($a->getWeight() < $b->getWeight()) ? -1 : 1;
        });

        $result = [];
        foreach ($terms as $term) {

            $result[] = [
                'name'   => $term->getName(),
                'src' => file_create_url($style->buildUrl($term->field_slide->entity->uri->value)),
            ];
        }

        return new JsonResponse($result);
    }

}